from backend.settings.base import *  # noqa

DEBUG = True
ALLOWED_HOSTS = ['*']

DATABASES = {
    'default': {
        'ENGINE': 'django.db.backends.sqlite3',
        'NAME': os.path.join(BASE_DIR, 'db.sqlite3'),  # noqa
    }
}

SECRET_KEY = '#-l1)dw@2_s119vzs@$hsz9le$#gccnf!9v50@%-5d7qvb3zdo'

EMAIL_BACKEND = 'django.core.mail.backends.console.EmailBackend'
