from backend.settings.base import *  # noqa

DEBUG = False

# TODO: Lots to move to .env here
ALLOWED_HOSTS = ['tkttest.tk', 'www.tkttest.tk']

SECRET_KEY = '#xl1)dw@rrrrrt@$hsz9le$#asdf!9v50@%-5d7qvb3zdo'

DATABASES = {
    'default': {
        'ENGINE': 'django.db.backends.postgresql_psycopg2',
        'NAME': 'tkttest_db',
        'USER': 'tkttest',
        'PASSWORD': '18djaghsed187312h3as61a',
        'HOST': 'localhost',
        'PORT': 5432,
    }
}

EMAIL_BACKEND = 'django.core.mail.backends.smtp.EmailBackend'
EMAIL_HOST = 'mail.privateemail.com'
EMAIL_PORT = 465
EMAIL_HOST_USER = 'hello@dylanh.net'
EMAIL_HOST_PASSWORD = '*bzdGd7?CtQaD'
EMAIL_USE_SSL = True
