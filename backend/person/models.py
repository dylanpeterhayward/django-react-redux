from django.db import models


class Person(models.Model):
    name = models.CharField(max_length=150)
    dob = models.DateField(blank=True)
    email = models.EmailField(blank=True, unique=True)
    created = models.DateTimeField(auto_now_add=True)
    modified = models.DateTimeField(auto_now=True)

    def __str__(self):
        return self.name

    class Meta:
        ordering = ("name", )
