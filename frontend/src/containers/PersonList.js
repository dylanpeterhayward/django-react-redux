import React from 'react';
import PersonCard from '../components/PersonCard.js';
import PersonCardControls from '../components/PersonCardControls.js';

const PersonList = props => (
  <div id="person-list">
    {props.people.map(person =>
      <PersonCard person={person} key={person.id}>
        <PersonCardControls id={person.id}/>
      </PersonCard>
    )}
  </div>
)

export default PersonList;
