import React from 'react';
import { render } from "react-dom";
import { Provider } from "react-redux";
import store from "./store/index.js";

import App from './components/App';
import './index.css';

import registerServiceWorker from './registerServiceWorker';


render(
  <Provider store={store}>
    <App />
  </Provider>,
  document.getElementById('root')
);
registerServiceWorker();
