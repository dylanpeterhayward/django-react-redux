import React, { Component } from 'react';
import { connect } from "react-redux";
import { flashMessage } from "../actions/misc.js";
import { api } from "../constants/api.js";
import { MESSAGES } from "../constants/common.js";
import store from '../store/index.js';

const mapStateToProps = state => {
  return {
    editModeElementID: state.editModeElementID,
    deleteModeElementID: state.deleteModeElementID,
   };
};


class ConnectedPersonCardControls extends Component {
  constructor(props) {
    super(props);
    this.state = {
      isEditModeActive: false,
      isDeleteModeActive: false,
    }
  }

  onCancel = () => {
    this.setState({
      isEditModeActive: false,
      isDeleteModeActive: false,
    })
  }

  onClickToUpdate = () => {
    this.setState({isEditModeActive: true})
  }

  onClickToDelete = () => {
    this.setState({isDeleteModeActive: true})
  }

  onConfirmUpdate = ()  => {
    api.one('person', this.props.id + '/').get().then(response => {
      const personEntity = response.body();
      const person = personEntity.data();

      if (this.props.edit.name !== "") {person.name = this.state.edit.name;}
      if (this.props.edit.dob !== "") {person.dob = this.state.edit.dob;}
      if (this.props.edit.email !== "") {person.email = this.props.edit.email;}

      personEntity.save().then(response => {
        //store.dispatch(resetState());

        this.setState({isEditModeActive: false})
        store.dispatch(flashMessage(
          MESSAGES.success,
          "Updated entry for " + person.name + "."
        ));

        //store.dispatch(refreshList());
      });
    })
  }

  onConfirmDelete = () => {
    api.one('person', this.props.id + '/').get().then(response => {
      const personEntity = response.body();
      const person = personEntity.data();
      personEntity.delete().then(response => {
        //store.dispatch(resetState());

        this.setState({isDeleteModeActive: false})

        store.dispatch(flashMessage(
          MESSAGES.success,
          "Deleted entry for " + person.name + "."
        ));

        //store.dispatch(refreshList());
      });
    })
  }

  render() {
    if (this.state.isEditModeActive) {
      return (
        <div className="person-card-menu">
          <button
            className="btn-cancel"
            onClick={this.onCancel}
          >
            Cancel
          </button>
          <button
            className="btn-confirm-update"
            onClick={this.onConfirmUpdate}
          >
            Save
          </button>
        </div>
      )
    } else if (this.state.isDeleteModeActive) {
      return (
        <div className="person-card-menu">
          <button
            className="btn-cancel"
            onClick={this.onCancel}
          >
            Cancel
          </button>
          <button
            className="btn-confirm-delete"
            onClick={this.onConfirmDelete}
          >
            Confirm
          </button>
        </div>

      )
    } else {
      return (
        <div className="person-card-menu">
          <button
            className="btn-update"
            onClick={this.onClickToUpdate}
          >
            Edit
          </button>
          <button
            className="btn-delete"
            onClick={this.onClickToDelete}
          >
            Delete
          </button>
        </div>
      )
    }
  }
}

const PersonCardControls = connect(mapStateToProps)(ConnectedPersonCardControls);

export default PersonCardControls;
