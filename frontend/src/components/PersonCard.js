import React, {Component} from 'react';
import { connect } from 'react-redux';

const mapStateToProps = state => {
  return {editModeElementID: state.editModeElementID};
};

class ConnectedPersonCard extends Component {
  isActiveEdit() {
    return (this.props.editModeElementID===this.props.person.id)
  }

  render() {
    return (
      <div className="person-card" >

        {this.props.children}

        {this.isActiveEdit
          ?
            <ul>
              <li>{this.props.person.name}</li>
              <li>{this.props.person.dob}</li>
              {this.props.person.email ? <li>{this.props.person.email}</li> : null}
            </ul>
          :
            <form>
              <input
                name="editName"
                type="text"
                onChange={this.handleEditChange}
                defaultValue={this.props.person.name}
              />
              <input
                name="editDob"
                type="date"
                onChange={this.handleEditChange}
                defaultValue={this.props.person.dob}
              />
              <input
                name="editEmail"
                type="email"
                onChange={this.handleEditChange}
                defaultValue={this.props.person.email}
              />
            </form>
        }
      </div>
    )
  }
}

const PersonCard = connect(mapStateToProps)(ConnectedPersonCard);

export default PersonCard;
