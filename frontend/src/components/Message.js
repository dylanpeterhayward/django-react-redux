import React from 'react';
import { connect } from "react-redux";

const mapStateToProps = state => {
  return { message: state.message };
};

const ConnectedMessage = ({ message }) => (
  <div id="message-box">
    <p className={message.level}>
      {message.text}
    </p>
  </div>
);

const Message = connect(mapStateToProps)(ConnectedMessage);

export default Message;
