import React, {Component} from 'react';
import { connect } from 'react-redux';
import store from '../store/index.js';
import { MESSAGES, API_PATH } from '../constants/common.js';
import { api } from '../constants/api.js';
import { flashMessage, toUrlEncoded } from '../actions/misc.js';

const mapStateToProps = state => {
  return { isCreateModeActive: state.isCreateModeActive };
};


class ConnectedCreateNew extends Component {
  constructor(props) {
    super(props);

    this.state = {
      name: '',
      dob: '',
      email: '',
    }
  }

  handleFormChange = (event) => {
    const name = event.target.name;
    this.setState({
      [name]: event.target.value
    })
  }

  handleFormSubmit = (event) => {
    this.setState({
      isFormPosting: true
    });

    const data = {
      name: this.state.name,
      dob: this.state.dob,
      email: this.state.email,
    }

    api.all(API_PATH).post(toUrlEncoded(data)).then(response => {
      store.dispatch(flashMessage(
        MESSAGES.success,
        'Added ' + this.state.name + ' to database.'
      ));
      //store.dispatch(resetState());
      //store.dispatch(refreshList());
    })

    event.preventDefault();
  }

  render() {
    if (this.props.isCreateModeActive) {
      return (
        <div id='person-create'>
          <form>
            <label>
              Name:
              <input
                name='name'
                type='text'
                onChange={this.handleFormChange}
                value={this.state.name}
              />
            </label>

            <label>
              Date:
              <input
                name='dob'
                type='date'
                onChange={this.handleFormChange}
                value={this.state.dob}
              />
            </label>

            <label>
              Email:
              <input
                name='email'
                type='email'
                onChange={this.handleFormChange}
                value={this.state.email}
              />
            </label>

            { this.state.isFormPosting
              ? (<p>Please wait...</p>)
              : (<button type='submit' onClick={this.handleFormSubmit}>Submit</button>)
            }
          </form>
        </div>
      )
    } else {
      return null
    }
  }
}

const CreateNew = connect(mapStateToProps)(ConnectedCreateNew);

export default CreateNew;
