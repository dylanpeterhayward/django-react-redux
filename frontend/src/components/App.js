import React, { Component } from 'react';

import Message from '../components/Message.js';
import CreateNew from '../components/CreateNew.js';
import PersonList from '../containers/PersonList.js';

import {setCreateMode, refreshList} from '../actions/misc.js';
import store from '../store/index.js';
import {refreshList} from '../helpers/api.js';

class App extends Component {
  constructor(props) {
    super(props);

    this.state = {
      people: [],
      activePerson: null,
      isFormPosting: null,
      name: "",
      dob: "",
      email: "",
      editModeElementID: null,
      deleteModeElementID: null,
      editName: "",
      editDob: "",
      editEmail: "",
      messageType: null,
      messageString: ""
    };
  }

  handleClickToCreate() {
    if (store.getState().isCreateModeActive) {
      store.dispatch(setCreateMode(false));
    } else {
      store.dispatch(setCreateMode(true));
    }
  }

  componentDidMount() {
    refreshList();
  }

  render() {

    return (
      <div className="App">

        <div id="nav-menu"><button onClick={this.handleClickToCreate}>+</button></div>
        <Message />
        <CreateNew />
        <PersonList people={this.state.people} />

      </div>
    );
  }
}

export default App;
