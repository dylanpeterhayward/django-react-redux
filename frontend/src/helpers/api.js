import restful, { requestBackend } from 'restful.js';
import {API_ROOT, API_HEADER} from '../constants/common.js';
import request from 'request';
import store from '../store/index.js';

const api = restful(API_ROOT, requestBackend(request));
api.header('Content-Type', API_HEADER);

export const fetchAll = () => {
  api.all(API_PATH).getAll().then(response => {
      const people = response.body().map(item => item.data());
      store.dispatch(refreshList(people));
  }).catch(error => {
    console.log(error);
  }
}
