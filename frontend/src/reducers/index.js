import {
  FLASH_MESSAGE,
  SET_CREATE_MODE,
  SET_DELETE_MODE,
  SET_EDIT_MODE,
  REFRESH_LIST,
} from "../constants/action-types";

const initialState = {
  message: {
    type: null,
    text: null,
  },
  isCreateModeActive: false,
  editModeElementID: null,
  deleteModeElementID: null,
  people: [],
};

const rootReducer = (state = initialState, action) => {
  switch (action.type) {
    case FLASH_MESSAGE:
      return { ...state, ...action.payload }
    case SET_CREATE_MODE:
      return { ...state, ...action.payload }
    case SET_EDIT_MODE:
      return { ...state, ...action.payload }
    case SET_DELETE_MODE:
      return { ...state, ...action.payload }
    case REFRESH_LIST:
      return { ...state, ...action.payload }
    default:
      return state;
  }
}

export default rootReducer;
