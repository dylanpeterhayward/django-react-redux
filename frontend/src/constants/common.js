export const API_ROOT = 'http://127.0.0.1:8000/api';
export const API_PATH = 'person/';
export const API_HEADER = 'application/x-www-form-urlencoded';

export const MESSAGES = {
  success: 'msg-success',
  warn: 'msg-warn',
  error: 'msg-error',
};
