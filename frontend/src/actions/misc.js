import {
  FLASH_MESSAGE,
  SET_CREATE_MODE,
  SET_DELETE_MODE,
  SET_EDIT_MODE,
  REFRESH_LIST,
} from "../constants/action-types.js";

import { API_PATH } from "../constants/common.js";
import { api } from "../constants/api.js";

// workaround for apparent problem with POST in Restful.js:
// https://github.com/marmelab/restful.js/issues/112
export const toUrlEncoded = obj => Object.keys(obj).map(k => encodeURIComponent(k) + '=' + encodeURIComponent(obj[k])).join('&');

export const flashMessage = (level, text) => ({
  type: FLASH_MESSAGE,
  payload: {
      message: {
        level: level,
        text: text
      }
    }
});

export const setCreateMode = newMode => ({
  type: SET_CREATE_MODE,
  payload: {
    isCreateModeActive: newMode
  }
});

export const setEditModeID = newID => ({
  type: SET_EDIT_MODE,
  payload: {
    editModeElementID: newID
  }
});

export const setDeleteModeID = newID => ({
  type: SET_DELETE_MODE,
  payload: {
    deleteModeElementID: newID
  }
});

export const refreshList = people => {
  type: REFRESH_LIST,
  payload: {people}
}
